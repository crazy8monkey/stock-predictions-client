import React from 'react';
import { Link } from 'react-router-dom'

import { GetTrade, UpdateTrade } from '../../services/trading.service';

class TradeView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      security: null,
      trade_purchase_date: null,
      trade_selling_date: null,
      data: null
    }
  }

  componentDidMount() {
    const { id } = this.props.match.params;
    GetTrade(id).then((response) => {
      this.setState({
        security: response.security,
        trade_purchase_date: response.trade_purchase_date,
        trade_selling_date: response.trade_selling_date,
        data: response.data
      });
    });
  }

  updateInfo = (event, info) => {
    const { data } = this.state;
    const { value } = event.target;
    data[info] = value;
    this.setState({ data: data });
  }

  saveTradeData = () => {
    const { data } = this.state;
    const { id } = this.props.match.params;
    let newData = {}
    Object.keys(data).forEach(function(key,index) {
      if(data[key] != "" && data[key]) {
        newData[key] = (!Number.isInteger(data[key]) ? parseFloat((Math.round(data[key] * 100) / 100)) : parseInt(data[key]))
      }
    });
    let newTradeData = {
      data:newData
    }

    UpdateTrade(id, newTradeData).then((response) => {

    });

    console.log('saved data => ', newData);
  }

  render() {
    const { security, trade_purchase_date, trade_selling_date, data } = this.state;
    return(
      <div className="container">
        <div className="row" style={{marginTop: "10px"}}>
          <div className="col">
            <nav aria-label="breadcrumb">
              <ol className="breadcrumb">
                <li className="breadcrumb-item">
                  <Link to="/trades">Trades</Link>
                </li>
                <li className="breadcrumb-item active" aria-current="page">Viewing Trade</li>
              </ol>
            </nav>
          </div>
        </div>
        <div className="row">
          <div className="col">
            <div>{security}</div>
            <div>Purchase Date: {new Date(trade_purchase_date).toLocaleDateString()}</div>
            <div>Sell Date: {new Date(trade_selling_date).toLocaleDateString()} </div>
          </div>
        </div>
        <div className="row" style={{marginTop: "20px"}}>
          <div className="col">
            <table>
              <tbody>
                <tr>
                  <td>Avg Volume</td>
                  <td>
                    {data != null &&
                      <input type="text" className="form-control" onChange={(e) => this.updateInfo(e, "Avg Volume")} value={data["Avg Volume"]} />
                    }
                  </td>
                </tr>
                <tr>
                  <td>Price To Earnings</td>
                  <td>
                    {data != null &&
                      <input type="text" className="form-control" onChange={(e) => this.updateInfo(e, "Price To Earnings")} value={data["Price To Earnings"]} />
                    }
                  </td>
                </tr>
                <tr>
                  <td>Price To Sales</td>
                  <td>
                    {data != null &&
                      <input type="text" className="form-control" onChange={(e) => this.updateInfo(e, "Price To Sales")} value={data["Price To Sales"]} />
                    }
                  </td>
                </tr>
                <tr>
                  <td>Price To Book</td>
                  <td>
                    {data != null &&
                      <input type="text" className="form-control" onChange={(e) => this.updateInfo(e, "Price To Book")} value={data["Price To Book"]} />
                    }
                  </td>
                </tr>
                <tr>
                  <td>Price Cash Per Share</td>
                  <td>
                    {data != null &&
                      <input type="text" className="form-control" onChange={(e) => this.updateInfo(e, "Price Cash Per Share")} value={data["Price Cash Per Share"]} />
                    }
                  </td>
                </tr>
                <tr>
                  <td>Return On Equity</td>
                  <td>
                    {data != null &&
                      <input type="text" className="form-control" onChange={(e) => this.updateInfo(e, "Return On Equity")} value={data["Return On Equity"]} />
                    }
                  </td>
                </tr>
                <tr>
                  <td>Debt To Equity</td>
                  <td>
                    {data != null &&
                      <input type="text" className="form-control" onChange={(e) => this.updateInfo(e, "Debt To Equity")} value={data["Debt To Equity"]} />
                    }
                  </td>
                </tr>
                <tr>
                  <td>Earnings Per Share</td>
                  <td>
                    {data != null &&
                      <input type="text" className="form-control" onChange={(e) => this.updateInfo(e, "Earnings Per Share")} value={data["Earnings Per Share"]} />
                    }
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div className="row">
          <div className="col">
            <button type="submit" className="btn btn-secondary" onClick={() => this.saveTradeData()}>Save Information</button>
          </div>
        </div>
      </div>
    )
  }
}

export default TradeView;

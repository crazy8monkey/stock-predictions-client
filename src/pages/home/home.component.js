import React from 'react';
import { Link } from 'react-router-dom'
import { SaveStock, GetStockList, RemoveStock } from '../../services/stock.service';

import './home.styles.scss';
import * as _ from 'lodash';

class Home extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      short_code: '',
      stocks:[]
    }
  }

  componentDidMount() {
    GetStockList().then((response) => {
      this.setState({ stocks: response });
    });
  }

  handleChange = event => {
    const { value, name } = event.target;
    this.setState({ [name]: value });
  };

  submitForm =  e => {
    const { short_code, stocks } = this.state;
    e.preventDefault();
    let newStock = {
      short_code
    }
    SaveStock(newStock).then((response) => {
      stocks.push(response);
      this.setState({ stocks: stocks });
    });
  };

  removeStock = (stock_id, event) => {
    const { short_code, stocks } = this.state;
    RemoveStock(stock_id).then((response) => {
      let stockIndex = _.findIndex(stocks, {id: stock_id});
      stocks.splice(stockIndex, 1);
      this.setState({ stocks: stocks });
    });
  }

  render() {
    const { stocks, short_code } = this.state;
      return (
        <div className="container">
          <form className="row g-3" style={{marginTop: '15px'}} onSubmit={this.submitForm}>
            <div className="col-auto">
              <label className="visually-hidden">Stock</label>
              <input type="text" className="form-control" id="stockName" name="short_code" value={short_code} onChange={this.handleChange} placeholder="Add Stock" />
            </div>
            <div className="col-auto">
              <button type="submit" className="btn btn-primary mb-3">Enter</button>
            </div>
          </form>
          <div className="row">
            <div className="col">
              <table className="table">
                <thead>
                  <tr>
                    <th scope="col">Security</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody>
                {stocks.map(stock => (
                  <tr key={stock.id}>
                    <td>{stock.short_code}</td>
                    <td>
                      <Link to={`/stock/${stock.id}`}>
                        <div className="button">
                          <i className="fas fa-caret-right"></i>
                        </div>
                      </Link>

                      <div className="button" onClick={event => this.removeStock(stock.id, event)}>
                        <i className="fas fa-trash"></i>
                      </div>
                    </td>
                  </tr>
                ))}
                </tbody>
              </table>
            </div>

          </div>
        </div>
      )
  }
}

export default Home;

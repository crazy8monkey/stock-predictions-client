import React from 'react';
import { AllTradingHistory, UpdateTradingRecord, GetTrades } from '../../services/trading.service';
import SecurityAnalysis from '../../classes/SecurityAnalysis';
import { Link } from 'react-router-dom'


import { connect } from 'react-redux';
import * as _ from 'lodash';

class Trades extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
     trades: [],
     trade_id: 0,
     current_price: 0,
     todays_date: null
    }
  }

  componentDidMount() {
    GetTrades().then((response) => {
      this.setState({ trades: response });
    });
  }

  render() {
    const { trades } = this.state;
    let analysis = new SecurityAnalysis();
    let currentTrades = trades.map(trade => {
      let trade_return = 0;
      if(trade.trade_return != 0) {
        trade_return = trade.trade_return
      }

      let tradeLine = {
        trade_id: trade.trade_id,
        security: trade.security,
        trade_purchase_date: new Date(trade.trade_purchase_date).toLocaleDateString(),
        trade_purchase_price:trade.trade_purchase_price.toFixed(2),
        trade_selling_date:(trade.trade_selling_date != null ?  new Date(trade.trade_selling_date).toLocaleDateString(): undefined),
        trade_selling_price:(trade.trade_selling_price != null ? `$${trade.trade_selling_price.toFixed(2)}` : undefined),
        trade_return:trade_return,
        trade_days_length:analysis.generateTradeDayHistory(trade.trade_purchase_date, trade.trade_selling_date)
      }
      return tradeLine;
    })

    return(
      <div className="container">
        <div className="row" style={{marginTop: "10px"}}>
          <div className="col">
            {trades.length > 0 &&
              <table className="table">
                <thead>
                  <tr>
                    <th scope="col">Security</th>
                    <th scope="col">Purchase Date</th>
                    <th scope="col">Price</th>
                    <th scope="col">Selling Date</th>
                    <th scope="col">Price</th>
                    <th scope="col">Cur. Rate of Return</th>
                    <th scope="col">Days of Trade</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody>
                  {currentTrades.map(trade => (
                    <tr key={trade.trade_id}>
                      <td>{trade.security}</td>
                      <td>{trade.trade_purchase_date}</td>
                      <td>${trade.trade_purchase_price}</td>
                      <td>{trade.trade_selling_date}</td>
                      <td>{trade.trade_selling_price}</td>
                      <td style={{color: trade.trade_return > 0 ? "green" : ""}}>
                        {trade.trade_return != 0 &&
                          <strong>{trade.trade_return}%</strong>
                        }
                      </td>
                      <td>{trade.trade_days_length}</td>
                      <td>
                        {trade.trade_selling_price != null &&
                          <Link to={`/trades/${trade.trade_id}`}>View</Link>
                        }
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            }
          </div>
        </div>
      </div>
    )
  }

}

export default Trades;

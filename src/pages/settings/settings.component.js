import React from 'react';
import { GetCurrentSettings, UpdateStockSettings } from '../../services/settings.service';


class Settings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      target_growth: null
    }

  }

  handleChange = event => {
    const { value, name } = event.target;
    this.setState({ [name]: value });
  };

  componentDidMount() {
    GetCurrentSettings().then((response) => {
      this.setState({ target_growth: response.target_growth });
    });
  }

  submitForm =  e => {
    const { target_growth } = this.state;
    e.preventDefault();
    let updatedSetting = {
      target_growth
    }
    UpdateStockSettings(updatedSetting).then((response) => {

    });
  };

  render() {
    return (
      <div className="container" style={{marginTop: '30px'}}>
        <div className="row">
          <div className="col-3">
            <ul class="list-group">
              <li class="list-group-item active" aria-current="true">Growth</li>
              <li class="list-group-item">Data</li>
            </ul>
          </div>
          <div className="col">
            <form className="row" style={{marginTop: '15px'}} onSubmit={this.submitForm}>
              <div className="col">
                <label className="form-label">Targeted Growth</label>
                <div className="input-group mb-3">
                  <input type="text" className="form-control" name="target_growth" value={this.state.target_growth} onChange={this.handleChange} placeholder="Stock Growth %"  />
                  <span className="input-group-text" id="basic-addon2">%</span>
                </div>
              </div>
              <div className="col-auto">
                <button type="submit" className="btn btn-primary mb-3">Enter</button>
              </div>
            </form>
          </div>
        </div>


      </div>
    )
  }


}

export default Settings;

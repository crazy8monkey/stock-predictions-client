import React from 'react';
import { Link } from 'react-router-dom'
import { GetStockType, AddStockTypeData, GetStockTypeData } from '../../services/stock-types.service';
import { v4 as uuidv4 } from 'uuid';
import * as _ from 'lodash';


class StockTypeView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: null,
      field_names:[]
    }

  }

  handleChange = event => {
    const { value, name } = event.target;
    this.setState({ [name]: value });
  };

  componentDidMount() {
    const { id } = this.props.match.params;
    GetStockType(id).then((response)=> {
      this.setState({name: response.name});
    });
    GetStockTypeData(id).then((response) => {
      this.setState({ field_names: response });
    });
  }

  submitAttributes =  e => {
    e.preventDefault();
    const { field_names } = this.state;
    const { id } = this.props.match.params;
    let data = field_names.map(field => {
      return {
        attribute:field.attribute,
        data:field.data,
        greater_lower:field.greater_lower
      }
    })
    AddStockTypeData(id, data).then((response) => {
        let updatedFieldNames = field_names.map((field, index) => {
          let fieldIndex = _.findIndex(response, (field) => {
            return field.attribute === response[index]['attribute'];
          });
          console.log(fieldIndex);
          return {
            id: fieldIndex !== -1 ? response[fieldIndex].id : field.id,
            attribute:field.attribute,
            data:field.data,
            greater_lower:field.greater_lower
          }
        });

        this.setState({field_names: updatedFieldNames});
    });
  };

  addToForm = event => {

    const { field_names } = this.state;
    let currentFieldForms = field_names;
    currentFieldForms.push({
      id: uuidv4(),
      attribute: '',
      data: '',
      greater_lower: ''
    });
    this.setState({field_names: currentFieldForms});
  }

  updateFieldNamesChange = (event, info) => {
    const { field_names } = this.state;
    const { value, name } = event.target;
    let updatedData = field_names.map(field => (field.id == info.id) ? { ...field, [name]: value } : field );
    this.setState({ field_names: updatedData });
  }

  removeFieldItem = (id) => {
    const { field_names } = this.state;
    const fieldNameIndex = _.findIndex(field_names, { id: id });
    field_names.splice(fieldNameIndex, 1);
    this.setState({ field_names: field_names });
  }

  render() {
    const { name, field_names } = this.state
    return (
      <div className="container" style={{marginTop: '30px'}}>
        <div className="row" style={{marginTop: "10px"}}>
          <div className="col">
            <nav aria-label="breadcrumb">
              <ol className="breadcrumb">
                <li className="breadcrumb-item">
                  <Link to="/stock-types">Stock Types</Link>
                </li>
                <li className="breadcrumb-item active" aria-current="page">Viewing Type</li>
              </ol>
            </nav>
          </div>
        </div>
        <div className="row">
          <div className="col">
            <h1>Viewing Type: {name}</h1>
          </div>
        </div>
        <div className="row" style={{marginBottom: "10px"}}>
          <div className="col">
            Current Attributes | <i className="fa fa-plus" onClick={this.addToForm} aria-hidden="true"></i>
          </div>
        </div>
        {field_names.length > 0 &&
          <form onSubmit={this.submitAttributes}>
            {field_names.map(type => (
              <div key={type.id} className="row" style={{marginBottom: "10px"}}>
                <div className="col">
                  <input type="text" className="form-control" name="attribute" onChange={(e) => this.updateFieldNamesChange(e, type)} placeholder="Fundamental" value={type.attribute} />
                </div>
                <div className="col">
                  <select className="form-select" value={type.greater_lower} name="greater_lower" onChange={(e) => this.updateFieldNamesChange(e, type)} aria-label="Default select example">
                    <option value="">Select Comparieson</option>
                    <option value=">">Greater than</option>
                    <option value="<">Less than</option>
                  </select>
                </div>
                <div className="col">
                  <input type="text" className="form-control" onChange={(e) => this.updateFieldNamesChange(e, type)} name="data" placeholder="Ratio" value={type.data} />
                </div>
                <div className="col">
                  <button type="button" onClick={(e) => this.removeFieldItem(type.id)}>
                    <i className="fa fa-trash" aria-hidden="true"></i>
                  </button>

                </div>
              </div>
            ))}
            <div className="row" style={{marginBottom: "10px"}}>
              <div className="col">
                <button type="submit" className="btn btn-primary mb-3">Enter</button>
              </div>
            </div>
          </form>
        }
      </div>
    )
  }
}

export default StockTypeView;

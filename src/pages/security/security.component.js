import React from 'react';
import { Link } from 'react-router-dom'
import { RetrieveStockData, GetStock, UpdateStock } from '../../services/stock.service';
import { GetStockData, UpdateSnapShot } from '../../services/snap-shot.service';
import { SaveTradingRecord, GetTradingHistory, GetTradeByStock, AddStockTrade, UpdateTradingRecord } from '../../services/trading.service';


import './security.styles.scss';
import { connect } from 'react-redux';
import SecurityAnalysis from '../../classes/SecurityAnalysis';
import * as _ from 'lodash';

class Security extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      periodic_growth: {},
      security: '',
      current_price: 0,
      three_month_close: 0,
      six_month_close: 0,
      nine_month_close:0,
      sector: null,
      market_cap: '',
      trade_purchase_date:null,
      trade_date: null,
      trade_history:[],
      snap_shot_data: [],
      ongoing_trade: false
    }
  }

  componentDidMount() {
    const { id } = this.props.match.params;
    GetStock(id).then((response) => {
      this.setState({
        security: response.short_code,
        sector: response.sector,
        market_cap: response.market_cap,
        current_price: response.current_price,
        three_month_close: response.three_month_close,
        six_month_close: response.six_month_close,
        nine_month_close: response.nine_month_close
      });

      GetStockData(response.short_code).then((response) => {
        this.setState({
          snap_shot_data: response
        });
      });

      GetTradeByStock(response.short_code).then((response) => {
        this.setState({
          trade_history: response,
          ongoing_trade: (_.findIndex(response, {trade_json: null}) != -1)
        });
      });
    });
  }


  getData = (e) => {
    const { id } = this.props.match.params;
    UpdateStock(id).then((response) => {
      this.setState({
        security: response.short_code,
        current_price: response.current_price,
        three_month_close: response.three_month_close,
        six_month_close: response.six_month_close,
        nine_month_close: response.nine_month_close
      });

      GetStockData(response.short_code).then((response) => {
        this.setState({
          snap_shot_data: response
        });
      });
    });
  }

  handleChange = event => {
    const { value, name } = event.target;
    this.setState({ [name]: value });
  };

  updateTrade = (event, trade) => {
    const { value, name } = event.target;

    const { trade_history } = this.state;
    let updatedData = trade_history.map(item => {
      if(item.trade_id == trade.trade_id) {
        return {
          ...item,
          [name]: value
        }
      } else {
        return item
      }
    });
    this.setState({ trade_history: updatedData });
  };


  updateSnapShotOnChange = (event, info) => {
    const { snap_shot_data } = this.state;
    const { value, name } = event.target;
    let updatedData = snap_shot_data.map(item => (item.id == info.id) ? { ...item, data: value } : item );
    this.setState({ snap_shot_data: updatedData });
  }

  saveStockData = () => {
    const { snap_shot_data, security } = this.state;

    let new_data = _.filter(snap_shot_data, (data) => { return data.update_type == "" });
    UpdateSnapShot(security, new_data).then((response) => {
      let updateData = snap_shot_data.map(item => {
        let newDataIndex = _.findIndex(response, {id: item.id});
        if(newDataIndex != -1) {
          return {
            ...item,
            data:response[newDataIndex].data,
            updated_date:response[newDataIndex].updated_date
          }
        } else {
          return item
        }
      });
      this.setState({ snap_shot_data: updateData });
    });

  }


  saveTrade = (e, trade, type) => {
    const { trade_history, security } = this.state;

    switch(type) {
      case "new":
        AddStockTrade(trade).then((response) => {
          trade_history.push(response);
          this.setState({
            trade_history: trade_history,
            ongoing_trade: true,
          });
        });
        break;
      case "update":
        UpdateTradingRecord(trade.trade_id, trade).then((response) => {
          let tradeIndex = _.findIndex(trade_history, {trade_id: trade.trade_id});
          trade_history[tradeIndex] = response;
          this.setState({
            trade_history: trade_history,
            ongoing_trade: false
          });
        });

        break;
    }
  }

  //saveNewTrade

  render() {
    const {
      trade_history,
      security,
      current_price,
      three_month_close,
      six_month_close,
      nine_month_close,
      snap_shot_data,
      trade_purchase_price,
      trade_purchase_date,
      sector,
      ongoing_trade
    } = this.state;
    let newTrade =  {
      trade_purchase_price,
      trade_purchase_date,
      trade_sector:sector,
      security
    }


    let analysis = new SecurityAnalysis();

    let currentTrades = trade_history.map(trade => {
      let trade_return = 0;
      if(trade.trade_return != 0) {
        trade_return = trade.trade_return
      } else {
        trade_return = analysis.calculateReturn(trade.trade_purchase_price, current_price)
      }

      //"en-US", { timeZone: "America/Chicago", hour12: true, hour: "2-digit", minute: "2-digit" }

      let tradeLine = {
        trade_id: trade.trade_id,
        security: trade.security,
        trade_purchase_date: new Date(trade.trade_purchase_date).toLocaleDateString(),
        trade_purchase_price:trade.trade_purchase_price.toFixed(2),
        trade_selling_date:(trade.trade_selling_date != null ?  new Date(trade.trade_selling_date).toLocaleDateString(): null),
        trade_selling_price: trade.trade_selling_price,
        trade_return:trade_return,
        trade_days_length:analysis.generateTradeDayHistory(trade.trade_purchase_date, trade.trade_selling_date)
      }
      return tradeLine;
    })

    return(
      <div className="container">
        <div className="row" style={{marginTop: "10px"}}>
          <div className="col">
            <nav aria-label="breadcrumb">
              <ol className="breadcrumb">
                <li className="breadcrumb-item">
                  <Link to="">Back To Home</Link>
                </li>
                <li className="breadcrumb-item active" aria-current="page">{security}</li>
              </ol>
            </nav>
          </div>
        </div>
        <div className="row">
          <div className="col">
            <h1>Full Analysis</h1>
          </div>
        </div>
        <div className="row">
          <div className="col">
            <button type="button" className="btn btn-secondary" onClick={this.getData}>Get Data</button>
          </div>
        </div>
        <div className="row" style={{marginTop: '20px'}}>
          <div className="col">
            <div className="card">
              <div className="card-body">
                <div className="row">
                  <div className="col">
                    <h2>Periodic Growth</h2>
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <div>3 Month Growth</div>
                    <div className="periodicGrowth">
                      {analysis.calculateReturn(three_month_close, current_price)}%
                    </div>
                  </div>
                  <div className="col">
                    <div>6 Month Growth</div>
                    <div className="periodicGrowth">
                      {analysis.calculateReturn(six_month_close, current_price)}%
                    </div>
                  </div>
                  <div className="col">
                    <div>9 Month Growth</div>
                    <div className="periodicGrowth">
                      {analysis.calculateReturn(nine_month_close, current_price)}%
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row" style={{marginTop: '20px'}}>
          <div className="col">
            <div className="card">
              <div className="card-body">
                <div className="row">
                  <div className="col">
                    <h2>Time Estimate Evaluation</h2>
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <table className="table table-striped">
                      <thead>
                        <tr>
                          <th scope="col">Data</th>
                          <th scope="col">Value</th>
                          <th scope="col">Est. Investment</th>
                          <th scope="col">Updated</th>
                        </tr>
                      </thead>
                      <tbody>
                      {snap_shot_data.map(info => (
                        <tr key={info.id}>
                          <td>{info.label}</td>
                          <td>
                            {info.update_type == "" &&
                              <input type="text" className="form-control" name="data" onChange={(e) => this.updateSnapShotOnChange(e, info)} value={info.data} />
                            }

                            {info.update_type != "" &&
                              <div>{info.data}</div>
                            }
                          </td>
                          <td></td>
                          <td>{new Date(info.updated_date).toLocaleDateString("en-US", { timeZone: "America/Chicago", hour12: true, hour: "2-digit", minute: "2-digit" }) }</td>
                        </tr>
                      ))}
                      </tbody>
                    </table>
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <button type="submit" style={{marginTop: '10px'}} className="btn btn-secondary" onClick={() => this.saveStockData()}>Save Information</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row" style={{marginTop: '20px'}}>
          <div className="col">
            <div className="card">
              <div className="card-body">
                <div className="row">
                  <div className="col">
                    <h2>Trade History</h2>
                  </div>
                </div>

                <div className="row">
                  <div className="col">
                    {currentTrades.length > 0 &&
                      <table className="table">
                        <thead>
                          <tr>
                            <th scope="col">Purchase Date</th>
                            <th scope="col">Price</th>
                            <th scope="col">Selling Date</th>
                            <th scope="col">Price</th>
                            <th scope="col">Cur. Rate of Return</th>
                            <th scope="col">Days of Trade</th>
                          </tr>
                        </thead>
                        <tbody>
                        {trade_history.map(trade => (
                          <tr key={trade.trade_id}>
                            <td>{trade.trade_purchase_date}</td>
                            <td>${trade.trade_purchase_price}</td>
                            <td>
                              {!ongoing_trade &&
                                <div>{trade.trade_selling_date}</div>
                              }
                              {ongoing_trade &&
                                <input type="date" className="form-control" name="trade_selling_date" value={trade.trade_selling_date || ''} onChange={(e) => this.updateTrade(e, trade)} placeholder="Selling Date" />
                              }
                            </td>
                            <td>
                              {!ongoing_trade &&
                                <div>${trade.trade_selling_price}</div>
                              }
                              {ongoing_trade &&
                                <input type="number" className="form-control" name="trade_selling_price" step="0.01" value={trade.trade_selling_price || ''} onChange={(e) => this.updateTrade(e, trade)} placeholder="Selling Price" />
                              }
                            </td>
                            <td>
                              {trade.trade_return != null &&
                                <strong style={{color: trade.trade_return > 0 ? "green" : "red"}}>{trade.trade_return}%</strong>
                              }
                              {ongoing_trade &&
                                <i onClick={(e) => this.saveTrade(e, trade, "update")} className="fas fa-save"></i>
                              }
                            </td>
                            <td>
                              {trade.trade_days_length}
                            </td>
                          </tr>
                        ))}
                        </tbody>
                      </table>
                    }
                  </div>
                </div>


              </div>
            </div>
          </div>
        </div>

        {!ongoing_trade &&
          <div className="row" style={{marginTop: '20px'}}>
            <div className="col">
            <div className="row">
              <div className="col">
                <input type="number" className="form-control" name="trade_purchase_price" step="0.01" onChange={this.handleChange} placeholder="Price" />
              </div>
              <div className="col">
                <input type="date" className="form-control" name="trade_purchase_date" onChange={this.handleChange} placeholder="Purcahse Date" />
              </div>
            </div>
            <button type="submit" style={{marginTop: '10px'}} className="btn btn-secondary" onClick={(e) => this.saveTrade(e, newTrade, "new")}>Add New Activity</button>
            </div>
          </div>
        }



      </div>
    )
  }
}

export default Security;
//export default connect(null, mapDispatch)(Security)

import React from 'react';
import { Link } from 'react-router-dom';
import { GetStockTypes, AddStockType } from '../../services/stock-types.service';


class StockTypes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: null,
      stock_types: []
    }

  }

  handleChange = event => {
    const { value, name } = event.target;
    this.setState({ [name]: value });
  };

  componentDidMount() {
    GetStockTypes().then((response)=> {
      this.setState({stock_types: response});
    });
  }

  submitForm =  e => {
    const { name, stock_types } = this.state;
    e.preventDefault();
    let newType = {
      name
    }
    let currentTypes = stock_types
    AddStockType(newType).then((response) => {
      currentTypes.push(response);
      this.setState({ stock_types: currentTypes });
    });
  };

  render() {
    const { name, stock_types } = this.state
    return (
      <div className="container" style={{marginTop: '30px'}}>
        <form className="row" style={{marginTop: '15px'}} onSubmit={this.submitForm}>
          <div className="row">
            <div className="col">
            <label className="form-label">Add New Stock Type</label>
              <input type="text" className="form-control" name="name" onChange={this.handleChange} placeholder="Stock Types"  />
            </div>
          </div>
          <div className="row" style={{marginTop: '15px'}}>
            <div className="col">
              <button type="submit" className="btn btn-primary mb-3">Enter</button>
            </div>
          </div>
        </form>
        <div className="row">
          <div className="col">
            <h1>Stock Types</h1>
          </div>
        </div>
        <div className="row">
          <div className="col">
            {stock_types.length == 0 &&
              <div>There is nothing entered</div>
            }
            {stock_types.length > 0 &&
              <table className="table">
                <thead>
                  <tr>
                    <th scope="col">Stock Type</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody>
                  {stock_types.map(type => (
                    <tr key={type.id}>
                      <td>{type.name}</td>
                      <td>
                        <Link to={`/stock-types/${type.id}`}>View</Link> | Delete
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            }
          </div>
        </div>


      </div>
    )
  }
}

export default StockTypes;

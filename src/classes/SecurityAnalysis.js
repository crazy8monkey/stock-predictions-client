class SecurityAnalysis {

  setData(new_data) {
    this.data = new_data
  }

  setCurrentPrice(price) {
    this.price = price;
  }

  calculateReturn(buy_price, sell_price) {
    let growth = parseFloat(sell_price) - parseFloat(buy_price);
    let growthPercentage = growth / buy_price;
    return (growthPercentage * 100).toFixed(2);
  }

  generateTradeDayHistory(purchase_date, selling_date) {
    var purchaseDate = new Date(purchase_date);
    var sellingDate = new Date();

    if(selling_date != null) {
      sellingDate = new Date(selling_date)
    }

    var ndays;
    var tv1 = purchaseDate.valueOf();  // msec since 1970
    var tv2 = sellingDate.valueOf();

    ndays = (tv2 - tv1) / 1000 / 86400;
    ndays = Math.round(ndays - 0.5);
    return ndays;
  }

}

export default SecurityAnalysis;

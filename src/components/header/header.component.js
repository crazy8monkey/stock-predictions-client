import React from 'react';
import { Link } from 'react-router-dom'

class Header extends React.Component {
  constructor(props) {
    super(props);

  }

  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <Link className="nav-link active" to="">Home</Link>
            </li>
          </ul>
          <ul className="navbar-nav d-flex">
            <li className="nav-item">
              <Link className="nav-link active" to="/trades">Trading History</Link>
            </li>
            <li className="nav-item">
              <div className="dropdown">
                <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                  Settings
                </button>
                <ul className="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuButton1">
                  <li>
                    <Link className="dropdown-item" to="/stock-types">Stock Data Types</Link>
                  </li>
                </ul>
              </div>
            </li>
          </ul>
        </div>
      </nav>
    )
  }
}

export default Header;

export const GetStockTypes = () => {
  return fetch(`/api/stock-types`, {
    method: "GET",
    headers: {
      'Content-Type': 'application/json'
    }
  }).then((resp) => resp.json())
}

export const AddStockType = (data) => {
  return fetch(`/api/stock-types`, {
    method: "POST",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then((resp) => resp.json())
}

export const GetStockType = (id) => {
  return fetch(`/api/stock-types/${id}`, {
    method: "GET",
    headers: {
      'Content-Type': 'application/json'
    }
  }).then((resp) => resp.json())
}

export const GetStockTypeData = (id) => {
  return fetch(`/api/stock-types/${id}/data`, {
    method: "GET",
    headers: {
      'Content-Type': 'application/json'
    }
  }).then((resp) => resp.json())
}


export const AddStockTypeData = (id, data) => {
  return fetch(`/api/stock-types/${id}`, {
    method: "POST",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then((resp) => resp.json())
};

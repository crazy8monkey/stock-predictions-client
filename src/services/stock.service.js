export const SaveStock = (data) => {
  return fetch(`/api/stock`, {
    method: "POST",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then((resp) => resp.json())
};

export const GetStockList = () => {
  return fetch(`/api/stock`, {
    method: "GET",
    headers: {
      'Content-Type': 'application/json'
    }
  }).then((resp) => resp.json())
}

export const UpdateStock = (stock_id) => {
  return fetch(`/api/stock/${stock_id}`, {
    method: "PUT",
    headers: {
      'Content-Type': 'application/json'
    }
  }).then((resp) => resp.json())
}

export const RemoveStock = (stock_id) => {
  return fetch(`/api/stock/${stock_id}`, {
    method: "DELETE",
    headers: {
      'Content-Type': 'application/json'
    }
  }).then((resp) => resp.json())
}

export const GetStock = (stock_id) => {
  return fetch(`/api/stock/${stock_id}`, {
    method: "GET",
    headers: {
      'Content-Type': 'application/json'
    }
  }).then((resp) => resp.json())
};

export const GetCurrentSettings = () => {
  return fetch(`/api/settings`, {
    method: "GET",
    headers: {
      'Content-Type': 'application/json'
    }
  }).then((resp) => resp.json())
}

export const UpdateStockSettings = (data) => {
  return fetch(`/api/settings`, {
    method: "PUT",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then((resp) => resp.json())
};

export const GetStockData = (security) => {
  return fetch(`/api/snap-shot/${security}`, {
    method: "GET",
    headers: {
      'Content-Type': 'application/json'
    }
  }).then((resp) => resp.json())
};

export const UpdateSnapShot = (security, data) => {
  return fetch(`/api/snap-shot/${security}`, {
    method: "PUT",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then((resp) => resp.json())
}

const baseUrl = "/api/trades";

export const GetTrades = () => {
  return fetch(`${baseUrl}`, {
    method: "GET",
    headers: {
      'Content-Type': 'application/json'
    },
  }).then((resp) => resp.json())
};

export const GetTrade = (id) => {
  return fetch(`${baseUrl}/${id}/data`, {
    method: "GET",
    headers: {
      'Content-Type': 'application/json'
    },
  }).then((resp) => resp.json())
};

export const UpdateTrade = (id, data) => {
  return fetch(`${baseUrl}/${id}/data`, {
    method: "PUT",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then((resp) => resp.json())
};


export const AddStockTrade = (data) => {
  return fetch(`${baseUrl}`, {
    method: "POST",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then((resp) => resp.json())
};

export const UpdateTradingRecord = (trade_id, data) => {
  return fetch(`${baseUrl}/${trade_id}`, {
    method: "PUT",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then((resp) => resp.json())
};

export const GetTradingHistory = (security) => {
  return fetch(`/api/trade/${security}`, {
    method: "GET",
    headers: {
      'Content-Type': 'application/json'
    }
  }).then((resp) => resp.json())
};

export const GetTradeByStock = (security) => {
  return fetch(`/api/trades/${security}`, {
    method: "GET",
    headers: {
      'Content-Type': 'application/json'
    }
  }).then((resp) => resp.json())
};

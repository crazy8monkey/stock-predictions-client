import React from 'react';
import './App.css';
import { Route, Switch } from 'react-router-dom'

import Home from './pages/home/home.component';
import Security from './pages/security/security.component';
import Settings from './pages/settings/settings.component';
import Trades from './pages/trades/trades.component';
import TradeView from './pages/trade-view/trade-view.component';
import StockTypes from './pages/stock-types/stock-types.component';
import StockTypeView from './pages/stock-type-view/stock-type-view.component';



class App extends React.Component {

  render() {
    return (
      <Switch>
        <Route exact path="/stock-types/:id" component={StockTypeView} />
        <Route exact path="/stock-types" component={StockTypes} />
        <Route exact path="/trades/:id" component={TradeView} />
        <Route exact path="/settings" component={Settings} />
        <Route exact path="/stock/:id" component={Security} />
        <Route exact path="/trades" component={Trades} />
        <Route exact path="" component={Home} />
      </Switch>
    );
  }
}
export default App;
